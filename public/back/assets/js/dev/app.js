;
(function (window, document, $, undefined) {
    'use strict';

    var app = (function () {

        var $private = {};
        var $public = {};
        
        //configuracao do plugin alertify para add as classes do twitter bootstrap
        $public.alertifyInit = function () {      
            alertify.defaults.transition = "slide";
            alertify.defaults.theme.ok = "btn btn-primary";
            alertify.defaults.theme.cancel = "btn btn-danger";
            alertify.defaults.theme.input = "form-control";
        };
        
        /*
         * os formularios com o atributo class formCookieSave terão 
         * o seu conteúdo salvo automaticamente
         * O objetivo é salvar a informação para ser usada quando o botão voltar do
         * navegador for clicado, assim o form vem preenchido e os datatables
         * fazem a consulta de acordo com o conteúdo do formulario, assim facilita
         * a vida do usuario final
         */
        $public.formCookieSave = function () {
            
            if ($('.formCookieSave').length > 0) {
                //faco um loop nos formularios que terao o conteudo salvo
                $('.formCookieSave').each(function(){
                   var form = $(this);
                   //grava o conteudo do form com as configuracoes padrao
                   //https://github.com/BenGriffiths/jquery-save-as-you-type
                   form.sayt();
                });
                
                //resetar o formulario e apagar os cookies
                $('.btnReset').on('click',function(){
                    var $this = $(this);
                    var form = $this.parents('form:first');
                    //limpa todos os campos do form
                    form.formReset();
                    form.sayt({'erase': true}); 
                });
            }
        };

        //valida os formularios do sistema
        $public.formValidate = function () {
            if ($('form').length > 0) {
                //form validator 
                $("form.formValidator").each(function(){
                    $(this).validate();
                });
            }
        };

        //ativa os tooltips do sistema
        $public.tooltipInit = function () {
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        };
        
        //seleciona todos os checkboxes
        $public.checkAll = function () {
            //selecionando todos os checkbox
            //funcao para selecionar todos os campos
            $('.checkall').on('click',function(){
              /*
               * pegando os campos que serao selecionados - eu indico isso atraves do atributo 
               * value do checkbox pai (que serve para selecionar).
               */
               var $this = $(this);
               var selecionar = $('.'+$this.val());
               //caso o checkbox esteja com o checked = true     
               if($this.is(':checked')){
                   //fazendo o loop pelos campos
                   $.each(selecionar,function(index,item){
                        $(item).attr('checked',true);
                        $(item).prop('checked', true); //prop foi colocado por causa do twitter bootstrap
                   });
               //caso o checkbox nao esteja com o checked = true     
               }else{
                   //fazendo o loop pelos campos
                   $.each(selecionar,function(index,item){
                         $(item).attr('checked',false);
                         $(item).prop('checked', false); //prop foi colocado por causa do twitter bootstrap
                   });
               }
            
            });
        };
        
        //faz o upload do arquivos
        $public.upload = function(){
            
            $('.upload').each(function(){
                var $this = $(this);
                var urlUpload = $this.data('url');
                var token = $this.data('token');
                var acceptFileTypes = $this.data('acceptFileTypes');
                var maxFileSize = $this.data('maxFileSize');
                var tableQueue = $('.fileUploadQueue');
                var indexPreview = 0;
                
                $this.fileupload({
                    url: urlUpload,
                    dataType: 'json',
                    autoUpload: false,
                    acceptFileTypes: acceptFileTypes,
                    maxFileSize: maxFileSize,
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    previewMaxWidth: 80,
                    previewMaxHeight: 80,
                    previewCrop: true,
                    headers: { 'X-XSRF-TOKEN' : token }, 
                    formData: {order: indexPreview},
                    }).on('fileuploadadd', function (e, data) {
                        
                        $.each(data.files, function (index, file) {
                            tableQueue.append($('<tr>\
                                    <td class="name">'+file.name+'</td>\\n\
                                    <td class="preview"></td>\
                                    <td class="size">'+file.size+'</td>\
                                    <td class="type">'+file.type+'</td>\
                                    <td class="status"></td>\
                                </tr>')
                            );
                        }); 
                        
                    }).on('fileuploadprocessalways', function (e, data) {

                        var index = data.index,
                            file = data.files[index];
                    
                        if (file.preview) {
                            var preview = tableQueue.find('tr').eq(indexPreview).find('td.preview');
                            $(preview).html(file.preview);
                        } 
                                          
                        /*
                        if (index + 1 === data.files.length) {
                            data.context.find('button')
                                .text('Upload')
                                .prop('disabled', !!data.files.error);
                        }*/
                        indexPreview++;    
                    }).on('fileuploadprogressall', function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                    }).on('fileuploaddone', function (e, data) {
                        $.each(data.result.files, function (index, file) {
                            if (file.url) {
                                var link = $('<a>')
                                    .attr('target', '_blank')
                                    .prop('href', file.url);
                                $(data.context.children()[index])
                                    .wrap(link);
                            } else if (file.error) {
                                var error = $('<span class="text-danger"/>').text(file.error);
                                $(data.context.children()[index])
                                    .append('<br>')
                                    .append(error);
                            }
                        });
                    }).on('fileuploadfail', function (e, data) {
                        $.each(data.files, function (index) {
                            var error = $('<span class="text-danger"/>').text('File upload failed.');
                            $(data.context.children()[index])
                                .append('<br>')
                                .append(error);
                        });
                    }).prop('disabled', !$.support.fileInput)
                      .parent().addClass($.support.fileInput ? undefined : 'disabled');
            });

       };

        
        return $public;
    })();

    // Global
    window.app = app;
    app.alertifyInit();
    app.formValidate();
    app.tooltipInit();
    app.checkAll();
    app.formCookieSave();
    app.upload();

})(window, document, jQuery);