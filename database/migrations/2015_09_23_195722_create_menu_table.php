<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name');
			$table->string('description')->nullable();
			$table->enum('status', array('A','I'))->default('A');
			$table->integer('order')->default(0);
			$table->integer('father_id')->nullable();
			$table->dateTime('datahorainc');
			$table->timestamp('datahoraalt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('link')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu');
	}

}
