<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRelUserTypeMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rel_user_type_menu', function(Blueprint $table)
		{
			$table->foreign('menu', 'fk_rel_user_type_menu_1')->references('id')->on('menu')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('type', 'fk_rel_user_type_menu_2')->references('id')->on('user_type')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rel_user_type_menu', function(Blueprint $table)
		{
			$table->dropForeign('fk_rel_user_type_menu_1');
			$table->dropForeign('fk_rel_user_type_menu_2');
		});
	}

}
