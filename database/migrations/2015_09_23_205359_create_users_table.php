<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email');
			$table->enum('status', array('A','I'))->default('A');
			$table->string('name');
			$table->dateTime('datahorainc');
			$table->timestamp('datahoraalt')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('password_reminder')->nullable();
			$table->integer('type')->index('users_type');
			$table->string('email2', 45)->nullable();
			$table->text('password', 65535);
			$table->text('remember_token', 65535)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
