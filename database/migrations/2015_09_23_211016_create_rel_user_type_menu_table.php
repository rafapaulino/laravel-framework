<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRelUserTypeMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rel_user_type_menu', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('type')->index('rel_user_type_type');
			$table->integer('menu')->index('rel_user_type_menu');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rel_user_type_menu');
	}

}
