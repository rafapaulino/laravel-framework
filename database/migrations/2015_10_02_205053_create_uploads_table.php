<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('uploads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('file');
			$table->string('original_name')->nullable();
			$table->integer('size')->nullable();
			$table->text('mime_type', 65535)->nullable();
			$table->string('path')->nullable();
			$table->timestamps();
			$table->string('extension', 10)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('uploads');
	}

}
