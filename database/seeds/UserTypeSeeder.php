<?php

use Illuminate\Database\Seeder;
use Modules\Admin\Entities\Users\UserType;

class UserTypeSeeder extends Seeder
{
    public function run()
    {
        DB::table('user_type')->delete();

        UserType::create([
            'type'=> 'User',
            'status'=> 'A',
            'created_at'=> date("Y-m-d H:i:s",time()),
            'description' => 'This is a normal user.'
        ]);

        UserType::create([
            'type'=> 'Administrator',
            'status'=> 'A',
            'created_at'=> date("Y-m-d H:i:s",time()),
            'description' => 'This is a administrator user.'
        ]);
    }
}
