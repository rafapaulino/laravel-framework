<?php

use Illuminate\Database\Seeder;
use Modules\Admin\Entities\Users\UserType;
use Modules\Admin\Entities\Users\Users;

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        
        $admin = UserType::where('type','Administrator')->first();
        $user = UserType::where('type','User')->first();
        
        Users::create([
            'email'=> 'john.doe@gmail.com',
            'email2'=> 'john.doe@outlook.com',
            'name'=> 'John Doe',
            'status'=> 'A',
            'password_reminder' => 'You is Ninja!',
            'created_at'=> date("Y-m-d H:i:s",time()),
            'type' => $admin->id,
            'password' => \Pwd::crypt('ninja')
        ]);
 
        Users::create([
            'email'=> 'tony.stark@gmail.com',
            'email2'=> 'tony.stark@outlook.com',
            'name'=> 'Tony Stark',
            'status'=> 'A',
            'password_reminder' => 'You is Iron Man!',
            'created_at'=> date("Y-m-d H:i:s",time()),
            'type' => $user->id,
            'password' => \Pwd::crypt('ironman')
        ]);
    }
}
