<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Administrator | @yield('title', 'Dashboard')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="https://www.google.com/fonts#UsePlace:use/Collection:Open+Sans:300,400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    @include('admin::assets.style_min')
    @yield('style')
</head>
<body>
    @yield('content')
    @include('admin::assets.scripts_min')
    @yield('script')
</body>
</html>