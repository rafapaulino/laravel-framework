@extends('admin::layouts.master')

@section('content')
<div class="container loginContainer">
    
    @if(Session::has('errors'))
    <div class="row">
       <div class="col-md-10 col-md-offset-1"> 
           @include('admin::partials.error')
       </div>
    </div>
    @endif
    
    @if(Session::has('success'))
    <div class="row">
       <div class="col-md-10 col-md-offset-1"> 
           @include('admin::partials.success')
       </div>
    </div>
    @endif
    
    <div class="row">
        <!-- login social -->
        <div class="col-md-5 col-md-offset-1">
            <h2>Register</h2>
            <p>Use Facebook, Twitter, GooglePlus or your email to register.</p>
            <br>
            <a class="btn btn-block btn-primary btnFacebook col-md-8" href="">
                <i class="fa fa-facebook"></i>
                <strong>Register with Facebook</strong> 
            </a>
            
            <a class="btn btn-block btn-primary btnTwitter col-md-8" href="">
                <i class="fa fa-twitter"></i>
                <strong>Register with Twitter</strong>
            </a>
            
            <a class="btn btn-block btn-danger btnGooglePlus col-md-8" href="">
                <i class="fa fa-google-plus"></i>
                <strong>Register with GooglePlus</strong>
            </a>
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p>You have an account? <a href="{{ route('admin') }}" title="Log in" data-toggle="tooltip" data-placement="top">Log in</a></p>
                </div>
            </div>
        </div>
        <!-- form login -->
        <div class="col-md-5"> 
            {!! Form::open(array('class' => 'formValidator')) !!}
            
                <div class="form-group">
                    <label class="form-label" for="name">Name</label>
                    {!! Form::text('name', null, array(
                         'class' => 'form-control', 
                         'required' => 'required',
                         'placeholder' => 'Type your name here...'
                    ))!!}
                </div>
            
                <div class="form-group">
                    <label class="form-label" for="email">Email</label>
                    {!! Form::email('email', null, array(
                         'class' => 'form-control', 
                         'required' => 'required',
                         'placeholder' => 'Type your email here...'
                    ))!!}
                </div>
            
                <div class="form-group">
                    <label class="form-label" for="password">Password</label>
                    {!! Form::password('password', array(
                         'class' => 'form-control', 
                         'required' => 'required',
                         'placeholder' => 'Type your password here...'
                    ))!!}
                </div>
            
                @include('admin::auth.captcha')
                
                <button type="submit" class="btn btn-block btn-success btnLogin">
                    <strong>Register</strong>
                </button>
                
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop