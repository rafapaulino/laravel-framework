@extends('admin::layouts.master')

@section('content')
<div class="container loginContainer">
    
    @if(Session::has('errors'))
    <div class="row">
       <div class="col-md-10 col-md-offset-1"> 
           @include('admin::partials.error')
       </div>
    </div>
    @endif
    
    @if(Session::has('success'))
    <div class="row">
       <div class="col-md-10 col-md-offset-1"> 
           @include('admin::partials.success')
       </div>
    </div>
    @endif   
    
    <div class="row">
        <!-- lost password -->
        <div class="col-md-5 col-md-offset-3">
            <h2>Set New Password</h2>
            <p>Enter the new password in the field below.</p>
            
                {!! Form::open(array(
                    'class' => 'formValidator',
                    'url' => route('admin.reset.password', ['token' => $token]),
                    'method' => 'post'
                )) !!}
                
                <div class="form-group">
                    <label class="form-label" for="password">Password</label>
                    {!! Form::password('password', array(
                         'class' => 'form-control', 
                         'required' => 'required',
                         'placeholder' => 'Type your password here...',
                         'id' => 'password'
                    ))!!}
                </div>
                
                <div class="form-group">
                    <label class="form-label" for="password_confirmation">Confirm Password</label>
                    {!! Form::password('password_confirmation', array(
                         'class' => 'form-control', 
                         'required' => 'required',
                         'equalTo' => '#password',
                         'placeholder' => 'Confirm your password...',
                         'id' => 'password_confirmation'
                    ))!!}
                </div>
                
                @include('admin::auth.captcha')
                
                <button class="btn btn-lg btn-success btn-block btnLogin" type="submit">Send</button>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop