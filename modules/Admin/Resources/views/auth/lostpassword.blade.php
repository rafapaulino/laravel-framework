@extends('admin::layouts.master')

@section('content')
<div class="container loginContainer">
    
    @if(Session::has('errors'))
    <div class="row">
       <div class="col-md-10 col-md-offset-1"> 
           @include('admin::partials.error')
       </div>
    </div>
    @endif
    
    @if(Session::has('success'))
    <div class="row">
       <div class="col-md-10 col-md-offset-1"> 
           @include('admin::partials.success')
       </div>
    </div>
    @endif
    
    
    <div class="row">
        <!-- lost password -->
        <div class="col-md-5 col-md-offset-3">
            <h2>Password Reset</h2>
            <p>Insert your email bellow and reset your password.</p>
            
                {!! Form::open(array(
                    'class' => 'formValidator',
                    'url' => route('admin.lost.password'),
                    'method' => 'post'
                )) !!}
                
                <div class="form-group">
                    <label class="form-label" for="email">Email</label>
                    {!! Form::email('email', null, array(
                         'class' => 'form-control', 
                         'required' => 'required',
                         'placeholder' => 'Type your email here...'
                    ))!!}
                </div>
                
                @include('admin::auth.captcha')
                
                <button class="btn btn-lg btn-success btn-block btnLogin" type="submit">Send me a reset link</button>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('script')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@stop