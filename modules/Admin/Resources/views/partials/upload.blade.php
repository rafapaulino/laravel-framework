<?php
    $encrypter = app('Illuminate\Encryption\Encrypter');
    $encrypted_token = $encrypter->encrypt(csrf_token());
?>
@extends('admin::layouts.master')

@section('content')
<form action="" method="get" enctype="multipart/form-data">
    

<!-- upload multiple -->
<div class="container-fluid">
  <div class="row">
      <div class="col-xs-12 col-md-12">
        
        <div class="panel-heading"> 
            <div class="panel-title">Add files to the upload queue and click the start button.</div> 
        </div>
        
        <div class="panel-body with-table">
            <table class="table table-bordered"> 
                <thead> 
                    <tr> 
                       <th>Filename</th> 
                       <th>Preview</th> 
                       <th>Size</th> 
                       <th>Type</th> 
                       <th>Status</th> 
                       <th>Actions</th> 
                    </tr> 
                </thead> 
                <tbody class="fileUploadQueue"> 
                    <!--
                    <tr>
                        <td class="name">darth-vader-01.jpg</td>
                        <td class="preview">darth-vader-01.jpg</td>
                        <td class="size">405 KB</td>
                        <td class="type">image/jpeg</td>
                        <td class="status"><i class="fa fa-check"></i></td>
                    </tr>
                    -->
                </tbody> 
                <tfoot> 
                    <tr> 
                        <td colspan="3">
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Add files...</span>
                                <input type="file" multiple="multiple" name="files[]" class="upload" data-url="{{ route('upload.enviar') }}" data-token="{{ $encrypted_token }}" data-acceptFileTypes="/(\.|\/)(gif|jpe?g|png|zip|doc|docx|xls|xlsx|pdf)$/i" data-maxFileSize="999000">
                            </span>
                            <button class="btn btn-primary start" type="submit">
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Start upload</span>
                            </button>
                            <button class="btn btn-warning cancel" type="reset">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancel upload</span>
                            </button>
                            <button class="btn btn-danger delete" type="button">
                                <i class="glyphicon glyphicon-trash"></i>
                                <span>Delete</span>
                            </button>
                        </td> 
                        <td colspan="2">
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-striped"></div>
                            </div>
                        </td>
                    </tr> 
                </tfoot> 
            </table>
        </div>
      </div>
  </div>
</div>
<!-- /upload multiple -->

 <br>
 <br>

 <!-- The container for the uploaded files -->
 <div id="files" class="files"></div>
 <br>
<ul id="filePreview"></ul>
</form>
@stop
