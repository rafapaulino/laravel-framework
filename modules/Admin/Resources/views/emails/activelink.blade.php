<p style="text-align: left;font-size:16px;">Hi {{ $name }},</p>

<p style="text-align: left;font-size:16px;">You are now part of our website, your access e-mail is: {{ $email }} and your password: {{ $password }}</p>

<p style="text-align: left;font-size:16px;">Please click on following link <a target="_blank" href="{{ route('admin') }}">Access Admin</a>.</p>

<p style="text-align: left;font-size:15px;">Sincerely,</p>

<p style="text-align: left;font-size:15px;">Support</p>