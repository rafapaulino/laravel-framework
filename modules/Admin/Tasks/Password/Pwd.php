<?php namespace Modules\Admin\Tasks\Password;

class Pwd
{
    //crypt password of application
    static public function crypt($password)
    {
        $hash = "";
        $salt = self::getSalt();
        $password = trim($password);
        
	foreach (hash_algos() as $v) {
            $hash.= hash($v, $password.$salt, false);
        }
        return sha1(md5($hash));
    }
    
    //return salt of application
    static private function getSalt()
    {
        return env('APP_SALT', 'A@#$Y89%vC$?dFQ!J6U');
    }
    
    static public function createPass($total = 6,$type = 'normal')
    {
        $varied = array('@','?','#','!','$','%','&',':','=','-','_');

        $numbers = array('1','2','3','4','5','6','7','8','9','0');

        $alphabet = array('A','a','B','b','C','c','D','d','E','e','F','f','G','g','H','h','I','i','J','j','K','k','L','l','M','m','N','n','O','o','P','p','Q','q','R','r','S','s','T','t','U','u','V','v','W','w','X','x','Y','y','Z','z');

        if($type == 'normal' || $type != 'varied')
        $pass_array = array_merge($numbers,$alphabet);
        else
        $pass_array = array_merge($numbers,$alphabet,$varied);

        shuffle($pass_array);

        //verificando o total de elementos
        $total_elements = count($pass_array);
        $half = $total_elements/2;
        $half = (int) $half;

        //verificando onde o array sera iniciado
        $secound = date("s",time());
        if($secound % 2 == 0){$beginning = 0;}else{$beginning = $half;}

        for($x=0; $x<$total; $x++)
        {
            $index = rand($beginning,($total_elements-1));
            $pass[$x] = $pass_array[$index];
        }

        $pass = implode("",$pass);

        return $pass;
    }
}