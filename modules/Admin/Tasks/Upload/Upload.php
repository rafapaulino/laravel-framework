<?php namespace Modules\Admin\Tasks\Upload;

use Input, File;
use Modules\Admin\Tasks\Slug\Slug;
use Modules\Admin\Entities\Upload\Upload as DbUpload;

class Upload 
{
    protected $_targetPath;
    
    protected $_publicUrl;
    
    protected $_path;
    
    protected $_slug;
   
    
    public function __construct()
    {
        $this->_slug = new Slug;       
        
        $this->_path = '/uploads/'.date("Y/m/",time());
        $this->_targetPath = public_path().$this->_path;
        $this->_publicUrl = \URL::to($this->_path).'/'; 
        @chmod(public_path().'/uploads/',0777);
        @mkdir($this->_targetPath, 0777, true);
    }
   

    public function moveFile()
    {
        $files = Input::all();
        $order = Input::get('order');
        $filesArray = array();
        foreach( $files as $file ) {
            foreach( $file as $f ) {

                $fileInfo = $this->getFileInfo($f);
                $fileInfo['order'] = intval($order);
                
                $f->move($this->_targetPath, $fileInfo['name']);
                
                $id = $this->insertFile($fileInfo);

                $filesArray[] = array(
                    'name' => $fileInfo['name'],
                    'url' => $this->_publicUrl.$fileInfo['name'],
                    'size' => $fileInfo['size'],
                    'thumbnailUrl' => '',
                    'deleteUrl' => '',
                    'deleteType' => 'DELETE',
                    'id' => $id,
                    'type' => 'type',
                    'order' => $fileInfo['order']
                );
            }
        }
        return $filesArray;
    }
    
    
    private function getFileInfo($file)
    {      
       $originalName = $file->getClientOriginalName(); 
       $extension = File::extension($originalName);
       $finalName = str_replace($extension,"",$originalName);
       $size = $file->getSize();
       $mimeType = $file->getMimeType();
       $filename = $this->_slug->slugify($finalName);
       $filename = date("Y-m-d-H-i-s",time()).'-'.$filename.'.'.$extension;
       
       return array(
           'name' => $filename,
           'extension' => $extension,
           'size' => $size,
           'mime_type' => $mimeType,
           'original_name' => $originalName
       );
    }
    
    
    private function insertFile($file)
    {
        $upload = new DbUpload;
        $upload->file = $file['name'];
        $upload->original_name = $file['original_name'];
        $upload->size = $file['size'];
        $upload->mime_type = $file['mime_type'];
        $upload->path = $this->_path;
        $upload->extension = $file['extension'];
        $upload->created_at = date("Y-m-d H:i:s",time());
        $upload->save();
        
        return $upload->id;
    }
    
}
