<?php namespace Modules\Admin\Tasks\Mailers;

abstract class Mailer {

    public function sendTo($email, $subject, $fromEmail, $view, $data = [])
    {
        \Mail::queue($view, $data, function($message) use($email, $subject, $fromEmail)
        {

            $message->from($fromEmail, env('APP_SUPORT_MAIL', ''));

            $message->to($email)
                ->subject($subject);
        });
    }
}