<?php namespace Modules\Admin\Tasks\Mailers;

class UserMailer extends Mailer 
{

    public function welcomeEmail($email, $data)
    {
        $view       = 'admin::emails.activelink';
        $subject    = $data['subject'];
        $fromEmail  = env('APP_SUPORT_MAIL', '');

        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

    public function passwordReset($email, $data)
    {
        $view       = 'admin::emails.passwordreset';
        $subject    = $data['subject'];
        $fromEmail  = env('APP_SUPORT_MAIL', '');

        $this->sendTo($email, $subject, $fromEmail, $view, $data);
    }

}