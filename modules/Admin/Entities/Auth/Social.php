<?php namespace Modules\Admin\Entities\Auth;
   
use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = [];
    
    protected $table = 'social_logins';
}