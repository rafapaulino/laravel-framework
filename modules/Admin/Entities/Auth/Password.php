<?php namespace Modules\Admin\Entities\Auth;

use Illuminate\Database\Eloquent\Model;

class Password extends Model 
{
    protected $table = 'password_resets';

    public $timestamps = false;
}
