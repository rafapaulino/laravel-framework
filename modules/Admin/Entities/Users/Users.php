<?php namespace Modules\Admin\Entities\Users;
   
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = [];
    
    protected $table = 'users';
}