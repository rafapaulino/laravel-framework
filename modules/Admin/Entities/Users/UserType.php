<?php namespace Modules\Admin\Entities\Users;
   
use Illuminate\Database\Eloquent\Model;

class UserType extends Model 
{
    protected $fillable = [];
    
    protected $table = 'user_type';
}