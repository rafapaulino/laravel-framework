<?php namespace Modules\Admin\Entities\Upload;
   
use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $fillable = [];
    
    protected $table = 'uploads';
}