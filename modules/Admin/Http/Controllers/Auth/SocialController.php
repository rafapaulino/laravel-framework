<?php namespace Modules\Admin\Http\Controllers\Auth;

use Pingpong\Modules\Routing\Controller;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Modules\Admin\Repositories\Auth\SocialRepository;


class SocialController extends Controller 
{  
    private $_social;
    
    public function __construct() 
    {
       $this->_social = new SocialRepository;
    }
    
    public function getSocialRedirect( $provider )
    {
        $providerKey = \Config::get('services.' . $provider);
        
        if (empty($providerKey)) {
           \Session::flash('errors', array('This social network is not registered in our system!')); 
           return redirect()->route('admin');           
        }

        return Socialite::driver( $provider )->redirect();
    }

    
    public function getSocialHandle( $provider )
    {
        $user = Socialite::driver( $provider )->user();

        $socialUser = $this->_social->loginSocial($user,$provider);
        
        if (!is_null($socialUser)) {
            
           Auth::loginUsingId( $socialUser, true );
           return redirect()->route('admin.lost.password');  
            
        } else {
          \Session::flash('errors', array('Unable to login, please try again later!'));  
        }
        
        return redirect()->route('admin');
    }
}