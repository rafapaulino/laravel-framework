<?php namespace Modules\Admin\Http\Controllers\Auth;

use Pingpong\Modules\Routing\Controller;
use Input, Auth;
use Illuminate\Http\Request;
use Modules\Admin\Repositories\Auth\PasswordRepository;
use Modules\Admin\Repositories\Auth\ResetRepository;

class PasswordController extends Controller 
{  
    private $_request;
       
    private $_password;
    
    private $_reset;
    
    public function __construct(Request $request) 
    {
       $this->_request = $request; 
       $this->_password = new PasswordRepository;
       $this->_reset = new ResetRepository;
    }
    
    public function lostpassword()
    {

        if ($this->_request->isMethod('post')) {
            $formData = Input::All();
            
            $this->_password->setFormData($formData);
            
            if ($this->_password->validate()) {
                return redirect()->route('admin');
            }
            
            return redirect()->route('admin.lost.password');
        }
        
        return view('admin::auth.lostpassword');
    }
    
    public function resetpassword( $token )
    {
        if ($this->_request->isMethod('post')) {
            $formData = Input::All();
            
            $this->_reset->setFormData($formData);
            
            if ($this->_reset->validate( $token )) {
                return redirect()->route('admin');
            }
            
            return redirect()->route('admin.reset.password', ['token' => $token]);
        }
        return view('admin::auth.resetpassword',array('token' => $token));
    }
}