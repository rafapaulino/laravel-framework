<?php namespace Modules\Admin\Http\Controllers\Auth;

use Pingpong\Modules\Routing\Controller;
use Input, Auth;
use Illuminate\Http\Request;
use Modules\Admin\Repositories\Auth\AuthUserRepository;
use Modules\Admin\Repositories\Auth\RegisterUserRepository;

class IndexController extends Controller 
{  
    private $_request;
    
    private $_auth;
    
    private $_register;
    
    public function __construct(Request $request) 
    {
       $this->_request = $request; 
       $this->_auth = new AuthUserRepository;
       $this->_register = new RegisterUserRepository;
    }
    
    public function index()
    {        
        if ($this->_request->isMethod('post')) {
            $formData = Input::All();
            
            $this->_auth->setFormData($formData);
            
            if ($this->_auth->validate()) {
                Auth::loginUsingId( $this->_auth->_userId, Input::has('remember') );
                return redirect()->route('admin.lost.password');
            }
            
            return redirect()->route('admin');
        }
  
        return view('admin::auth.login');
    }

    public function register()
    {
        if ($this->_request->isMethod('post')) {
            $formData = Input::All();
            
            $this->_register->setFormData($formData);
            
            if ($this->_register->validate()) {
                return redirect()->route('admin');
            }
            
            return redirect()->route('admin.register');
        }
        return view('admin::auth.register');
    }
    
    public function logout()
    {
        \Auth::logout();
        return redirect()->route('admin');
    }
}