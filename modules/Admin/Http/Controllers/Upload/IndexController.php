<?php namespace Modules\Admin\Http\Controllers\Upload;

use Pingpong\Modules\Routing\Controller;
use Modules\Admin\Tasks\Upload\Upload;

class IndexController extends Controller 
{  
    
    public function index()
    {        
        return view('admin::partials.upload');
    }
    
    public function upload()
    {
        $json = array();
        $upload = new Upload;
        $data = $upload->moveFile(); 
        
        if (count($data) > 0) {
            $json['files'] = $data;
        }
        return response()->json($json);
    }        
          
}