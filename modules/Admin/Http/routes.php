<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    //login page
    Route::match(['get', 'post'], '/', 
        array(
            'as' => 'admin',
            'uses' => 'Auth\IndexController@index'
        )
    );
    
    //lost password
    Route::match(['get', 'post'], '/lost-password', 
        array(
            'as' => 'admin.lost.password',
            'uses' => 'Auth\PasswordController@lostpassword'
        )
    );
    
    //password reset
    Route::match(['get', 'post'], '/reset-password/{token}', 
        array(
            'as' => 'admin.reset.password',
            'uses' => 'Auth\PasswordController@resetpassword'
        )
    );
    
    //register
    Route::match(['get', 'post'], '/register', 
        array(
            'as' => 'admin.register',
            'uses' => 'Auth\IndexController@register'
        )
    );
    
    //logout
    Route::match(['get'], '/logout', 
        array(
            'as' => 'admin.logout',
            'uses' => 'Auth\IndexController@logout'
        )
    );
    
    //login social
    Route::get('/social/redirect/{provider}',   
        array(
            'as' => 'social.redirect',   
            'uses' => 'Auth\SocialController@getSocialRedirect'
        )
    );
    
    Route::get('/social/handle/{provider}',     
        array(
            'as' => 'social.handle',     
            'uses' => 'Auth\SocialController@getSocialHandle'
        )
    );
    
    //upload
    Route::match(['get', 'post'], '/upload', 
        array(
            'as' => 'upload',
            'uses' => 'Upload\IndexController@index'
        )
    );
    
    Route::match(['get', 'post'], '/upload/enviar', 
        array(
            'as' => 'upload.enviar',
            'uses' => 'Upload\IndexController@upload'
        )
    );
});