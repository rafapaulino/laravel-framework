<?php namespace Modules\Admin\Repositories\Auth;

use Validator, Carbon\Carbon;
use Modules\Admin\Entities\Users\Users;
use Modules\Admin\Entities\Auth\Password;
use Modules\Admin\Traits\CaptchaTrait;

class ResetRepository 
{
   private $_rules; 
   
   private $_messages;
   
   private $_formData;
   
   use CaptchaTrait;
   
   public function __construct() 
   {
        $this->_rules = array(
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|same:password'        
        );

        $this->_messages = array(
            'password.required' => 'Password is required.',
            'password.min' => 'Password needs to have at least 6 characters.',
            'password_confirmation.required' => 'Confirm password is required.',
            'password_confirmation.same' => 'Repeat the previous field.'
        );
   }
   
   public function setFormData($formData)
   {
        $this->_formData = $formData;
   }
   
   public function validate( $token )
   {
        $validation = Validator::make($this->_formData, $this->_rules, $this->_messages);
        $isValid = $validation->passes();
        
        //if is valid
        if($isValid) {
            
            //verify captcha
            if($this->captchaCheck($this->_formData['g-recaptcha-response']) == false) {
                \Session::flash('errors', array('Wrong Captcha!'));
            } else {          
                return $this->resetPass($token);
            }
            
        } else {
            \Session::flash('errors', $validation->messages()->all(':message'));
        }
        return false;
   }
   
   private function resetPass( $token )
   {
        //verify if token is valid 
        $yesterday = Carbon::yesterday();

        $password = Password::where('token', '=', $token)
                ->where('created_at', '>', $yesterday)
                ->first();

        if(empty($password)) {
            \Session::flash('errors', array('Your token has expired or does not exist.')); 
        } else {

            $pass = $this->_formData['password'];
            $crypt = \Pwd::crypt($pass);

            $user = Users::where('id', '=', $password->user)->first();
            $user->password = $crypt;
            $user->save();

            //delete all tokens of this user
            $password = Password::where('user', '=', $password->user);
            $password->delete();

            \Session::flash('success', array('Password changed successfully!'));
            return true;
        } 
        return false;
   }
}
