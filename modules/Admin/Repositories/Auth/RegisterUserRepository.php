<?php namespace Modules\Admin\Repositories\Auth;

use Validator;
use Modules\Admin\Entities\Users\Users;
use Modules\Admin\Entities\Users\UserType;
use Modules\Admin\Tasks\Mailers\UserMailer;
use Modules\Admin\Traits\CaptchaTrait;

class RegisterUserRepository 
{
   private $_rules; 
   
   private $_messages;
   
   private $_formData;
   
   protected $userMailer;
   
   use CaptchaTrait;
    
   public function __construct() 
   {
        $this->_rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',         
        );
       
        $this->_messages = array(
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.email' => 'Email is invalid.',
            'password.required' => 'Password is required.',
            'password.min' => 'Password needs to have at least 6 characters.'
        ); 
        
        $this->userMailer = new UserMailer;
   }
   
   public function setFormData($formData)
   {
       $this->_formData = $formData;
   }
   
   public function validate()
   {
        $validation = Validator::make($this->_formData, $this->_rules, $this->_messages);
        $isValid = $validation->passes();
        
        //create user
        if($isValid) {
           
            //verify captcha
            if($this->captchaCheck($this->_formData['g-recaptcha-response']) == false) {
                \Session::flash('errors', array('Wrong Captcha!')); 
            } else {
                //create user
                if ($this->createUser()) {
                  \Session::flash('success', array('You are registered successfully. Please login.'));
                  return true;              
                } else {
                  \Session::flash('errors', array('An error occurred during your registration, please try again!')); 
                }
            }
               
        //inform about error message
        } else {
           \Session::flash('errors', $validation->messages()->all(':message'));
        }
        return false;
   }
   
   public function createUser()
   {
      $password = $this->_formData['password'];
      $crypt = \Pwd::crypt($password);
      
      //get type of user
      $type = UserType::where('type','User')->first();
      
      $user = new Users;
      $user->email = $this->_formData['email'];
      $user->name = $this->_formData['name'];
      $user->password = $crypt;
      $user->status = 'A';
      $user->type =  $type->id;
      $user->save();
      
      //send email
      $data = [
        'name' => $this->_formData['name'],
        'email' => $this->_formData['email'],
        'password' => $this->_formData['password'],
        'subject' => 'Welcome to Admin',
      ];
                      
      $this->userMailer->welcomeEmail($user->email, $data); 
      
      return true;
   }
}
