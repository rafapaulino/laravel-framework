<?php namespace Modules\Admin\Repositories\Auth;

use Modules\Admin\Entities\Users\Users;
use Modules\Admin\Entities\Auth\Social;
use Modules\Admin\Tasks\Mailers\UserMailer;
use Carbon\Carbon;


class SocialRepository 
{  
    
   private $_userId;
   
   private $userMailer;
   
   public function __construct() 
   {
       $this->_userId = null;
       
       $this->userMailer = new UserMailer;
   }
   
   public function loginSocial( $user, $provider )
   {
        //Check is this email present
        $userCheck = Users::where('email', '=', $user->email)->first();
       
        if(!empty($userCheck))
        {
            $this->_userId = $userCheck->id;
        }
        else
        {
            $sameSocialId = Social::where('social_id', '=', $user->id)->where('provider', '=', $provider )->first();
            
            if(empty($sameSocialId))
            {
                //get type of user
                $type = UserType::where('type','User')->first();
                $password = \Pwd::createPass();
                $crypt = \Pwd::crypt($password);
                
                //There is no combination of this social id and provider, so create new one
                $newSocialUser = new Users;
                $newSocialUser->email = $user->email;
                $newSocialUser->name = $user->name;
                $newSocialUser->password = $crypt;
                $newSocialUser->status = 'A';
                $newSocialUser->type =  $type->id;
                $newSocialUser->save();

                $socialData = new Social;
                $socialData->user_id = $newSocialUser->id;
                $socialData->provider = $provider;
                $socialData->created_at = Carbon::now();
                $socialData->social_id = $user->id;
                $socialData->save();
                
                $this->_userId = $newSocialUser->id;
                
                //send email with password for user
                $data = [
                  'name' => $user->name,
                  'email' => $user->email,
                  'password' => $password,
                  'subject' => 'Welcome to Admin',
                ];
                      
                $this->userMailer->welcomeEmail($user->email, $data); 
            }
            else
            {
                //Load this existing social user
                $this->_userId = $sameSocialId->user_id;
            }
        }
        
        return $this->_userId;
   }
}
