<?php namespace Modules\Admin\Repositories\Auth;

use Validator, Carbon\Carbon;
use Modules\Admin\Entities\Users\Users;
use Modules\Admin\Entities\Auth\Password;
use Modules\Admin\Tasks\Mailers\UserMailer;
use Modules\Admin\Traits\CaptchaTrait;

class PasswordRepository 
{
   private $_rules; 
   
   private $_messages;
   
   private $_formData;
   
   protected $userMailer;

   use CaptchaTrait;
  
   public function __construct() 
   {
        $this->_rules = array(
            'email' => 'required|email'        
        );

        $this->_messages = array(
            'email.required' => 'Email is required.',
            'email.email' => 'Email is invalid.'
        );
        
        $this->userMailer = new UserMailer;
   }
   
   public function setFormData($formData)
   {
        $this->_formData = $formData;
   }
   
   public function validate()
   {
        $validation = Validator::make($this->_formData, $this->_rules, $this->_messages);
        $isValid = $validation->passes();
        
        //if is valid, verify if user exists
        if($isValid) {
            
            //verify captcha
            if($this->captchaCheck($this->_formData['g-recaptcha-response']) == false) {
                \Session::flash('errors', array('Wrong Captcha!'));
            } else {          
                return $this->createToken();
            }
            
        } else {
            \Session::flash('errors', $validation->messages()->all(':message'));
        }
        return false;
   }
   
   private function createToken()
   {
        $email  = $this->_formData['email'];
        $user   = Users::where('email', '=', $email)
                ->where('status','=','A')
                ->first(); 
        
        if(empty($user)) {
            \Session::flash('errors', array('User with this email does not exist')); 
        } else {
                
            $token = sha1(mt_rand());
            $password = new Password;
            $password->email = $user->email;
            $password->token = $token;
            $password->created_at = Carbon::now();
            $password->user = $user->id;
            $password->save();

            $data = [
                'name'    => $user->name,
                'token'   => $token,
                'subject' => 'Example.com: Password Reset Link',
                'email'   => $user->email
            ];

            $this->userMailer->passwordReset($user->email, $data);   
                
            \Session::flash('success', array('An email was sent with instructions to change your password, please check your mailbox!'));
            return true;
        }
        return false;
   }
}
