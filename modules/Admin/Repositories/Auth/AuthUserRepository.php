<?php namespace Modules\Admin\Repositories\Auth;

use Validator;
use Modules\Admin\Entities\Users\Users;
use Modules\Admin\Traits\CaptchaTrait;

class AuthUserRepository 
{
   private $_rules; 
   
   private $_messages;
   
   private $_formData;
   
   private $_userId;
   
   use CaptchaTrait;
    
   public function __construct() 
   {
        $this->_rules = array(
            'email' => 'required',
            'password' => 'required'           
        );
       
        $this->_messages = array(
            'email.required' => 'Email is required.',
            'password.required' => 'Password is required.'
        ); 
   }
   
   public function setFormData($formData)
   {
       $this->_formData = $formData;
   }
   
   public function validate()
   {
        $validation = Validator::make($this->_formData, $this->_rules, $this->_messages);
        $isValid = $validation->passes();
        
        //verify is valid
        if(!$isValid) {
            \Session::flash('errors', $validation->messages()->all(':message'));
            return false;
        //verify captcha
        } elseif($this->captchaCheck($this->_formData['g-recaptcha-response']) == false) {
          \Session::flash('errors', array('Wrong Captcha!')); 
          return false;            
        //verify if exists 
        } else {
           return $this->exists(); 
        }
   }
   
   public function exists()
   {
      $password = $this->_formData['password'];
      $crypt = \Pwd::crypt($password);
      
      $user = Users::where('email','=',$this->_formData['email'])
              ->where('password','=',$crypt)
              ->where('status','=','A')
              ->orWhere('email2','=',$this->_formData['email'])
              ->where('password','=',$crypt)
              ->where('status','=','A')
              ->first();
      
      if ($user) {
          $this->_userId = $user->id;
          return true;
      } else {
          \Session::flash('errors', array('User not found!')); 
          return false;
      }
      
   }

   public function __get($property)
   {
      if (property_exists($this, $property)) {
        return $this->$property;
      }
   }
}
