<?php namespace Modules\Admin\Facades;

use Illuminate\Support\Facades\Facade;

class PwdFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'pwd'; }
}
